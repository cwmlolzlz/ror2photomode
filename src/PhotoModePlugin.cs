using BepInEx;
using RoR2;
using RoR2.UI;
using RoR2.UI.SkinControllers;
using R2API.Utils;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace PhotoMode {
    
    [BepInDependency ("com.bepis.r2api")]
    [BepInPlugin ("com.cwmlolzlz.photomode", "PhotoMode", "1.1.0")]
    [NetworkCompatibility(CompatibilityLevel.NoNeedForSync)]
    public class PhotoModePlugin : BaseUnityPlugin {

        // public PhotoModeController photoModeController;
        public CameraRigController cameraRigController;
        
        public void Awake () {

            On.RoR2.CameraRigController.OnEnable += (orig, self) => {
                orig(self);
                this.cameraRigController = self;
            };

            On.RoR2.CameraRigController.OnDisable += (orig, self) => {
                orig(self);
                this.cameraRigController = null;
            };

            On.RoR2.UI.PauseScreenController.Awake += (orig, self) => {
                orig(self);
                SetupPhotoModeButton(self);
            };
        }

        private void SetupPhotoModeButton(PauseScreenController pauseScreenController) {
            GameObject otherGameObject = pauseScreenController.GetComponentInChildren<ButtonSkinController>().gameObject;
            GameObject photoModeButtonGameObject = Instantiate(otherGameObject, otherGameObject.transform.parent);
            photoModeButtonGameObject.name = "GenericMenuButton (Photo mode)";
            photoModeButtonGameObject.transform.SetSiblingIndex(1);
            
            ButtonSkinController buttonSkinController = photoModeButtonGameObject.GetComponent<ButtonSkinController>();
            buttonSkinController.GetComponent<LanguageTextMeshController>().token = "Photo mode";

            HGButton hgButton = photoModeButtonGameObject.GetComponent<HGButton>();
            hgButton.interactable = RoR2Application.isInSinglePlayer && cameraRigController.localUserViewer != null;

            // clear all existing listeners
            for(int i = 0; i < hgButton.onClick.GetPersistentEventCount(); i++) {
                Debug.Log(hgButton.onClick.GetPersistentMethodName(i).ToString());
                hgButton.onClick.SetPersistentListenerState(i, UnityEngine.Events.UnityEventCallState.Off);
            }

            hgButton.onClick.AddListener(delegate() {
                GameObject gameObject = new GameObject("PhotoModeController");
                PhotoModeController photoModeController = gameObject.AddComponent<PhotoModeController>();
                photoModeController.EnterPhotoMode(pauseScreenController, cameraRigController);
            });
        }

    }
}