using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using UnityEngine;
using R2API;
using R2API.Utils;
using Rewired;
using RoR2;
using RoR2.UI;

namespace PhotoMode {

    class PhotoModeController: MonoBehaviour, ICameraStateProvider {

        internal struct PhotoModeCameraState {

            internal float pitch;
            internal float yaw;
            internal float roll;

            internal Quaternion Rotation {
                get { return Quaternion.Euler(pitch, yaw, roll); }
                set {
                    Vector3 eulerAngles = value.eulerAngles;
                    this.pitch = eulerAngles.x;
                    this.yaw = eulerAngles.y;
                    this.roll = eulerAngles.z;
                }
            }

            internal Vector3 position;

            internal float fov;

        }

        private static float cameraSpeed = 10f;
        private static float cameraSprintMultiplier = 5f;
        private static float cameraSlowMultiplier = 0.3f;

        public CameraRigController cameraRigController;
        public PauseScreenController pauseController;
        private PhotoModeCameraState cameraState;

        private event EmptyDelegate OnExit;

        private Camera Camera {
            get { return cameraRigController.sceneCam; }
        }

        internal void EnterPhotoMode(PauseScreenController pauseController, CameraRigController cameraRigController) {

            this.pauseController = pauseController;
            this.cameraRigController = cameraRigController;

            Debug.Log("Entering photo mode");
            this.pauseController.gameObject.SetActive(false);
            if(this.cameraRigController) {
                this.cameraRigController.SetOverrideCam(this, 0.0f);
                this.cameraRigController.enableFading = false;
            }
            this.cameraState = new PhotoModeCameraState();
            this.cameraState.position = this.Camera.transform.position;
            this.cameraState.Rotation = Quaternion.LookRotation(this.Camera.transform.rotation * Vector3.forward, Vector3.up);
            
            this.cameraState.fov = this.Camera.fieldOfView;
            Time.timeScale = 0.0f;
            if(SettingsConVars.enableDamageNumbers.value) {
                SettingsConVars.enableDamageNumbers.SetBool(false);
                this.OnExit += () => SettingsConVars.enableDamageNumbers.SetBool(true);
            }
            if(SettingsConVars.cvExpAndMoneyEffects.value) {
                SettingsConVars.cvExpAndMoneyEffects.SetBool(false);
                this.OnExit += () => SettingsConVars.cvExpAndMoneyEffects.SetBool(true);
            }

            this.cameraRigController.hud.mainContainer.GetComponent<Canvas>().enabled = false;
            this.OnExit += () => this.cameraRigController.hud.mainContainer.GetComponent<Canvas>().enabled = true;

            this.SetIndicatorsVisible(false);
            this.OnExit += () => this.SetIndicatorsVisible(true);

            SetupPhotoModeHUD(this.cameraRigController.hud);
        }

        private void SetupPhotoModeHUD(HUD hud) {
            GameObject hudGameObject = hud.gameObject;
            GameObject photoModeHDGameObject = new GameObject("PhotoModeHUD", typeof(Canvas));
        }

        private void ExitPhotoMode() {
            this.OnExit();

            this.pauseController.gameObject.SetActive(true);
            if(this.cameraRigController) {
                this.cameraRigController.enableFading = true;
                this.cameraRigController.SetOverrideCam(null, 1f);
            }
            Debug.Log("Exiting photo mode");
            Time.timeScale = 1f;
            this.Camera.transform.localPosition = Vector3.zero;
            this.Camera.transform.localRotation = Quaternion.identity;
            Destroy(this.gameObject);
        }

        private void SetIndicatorsVisible(bool visible) {
            Type indicatorManagerType = typeof(Indicator).GetNestedType("IndicatorManager", BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
            if(indicatorManagerType == null) {
                Debug.Log("Failed to find indicatorManagerTypeInfo");
            } else {
                FieldInfo field = indicatorManagerType.GetField("runningIndicators", BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
                if(field == null) {
                    Debug.Log("Failed to find runningIndicatorsFieldInfo");
                } else {
                    Debug.Log(field.ToString());
                    object indicatorInstances = field.GetValue(null);
                    if(indicatorInstances is List<Indicator>) {
                        ((List<Indicator>)indicatorInstances).ForEach(indicator => indicator.SetVisualizerInstantiated(visible));
                    }
                    if(!visible) {
                        DamageNumberManager.instance.GetFieldValue<ParticleSystem>("ps").Clear(true);
                    }
                }
            }
            this.cameraRigController.hud.combatHealthBarViewer.enabled = visible;
            PingIndicator.instancesList.ForEach(pingIndicator => { pingIndicator.gameObject.SetActive(visible); });

            if(!visible) {
                this.cameraRigController.sprintingParticleSystem.Clear(true);
            }
        }

        private void Update() {
            if(Input.GetKeyDown(KeyCode.Escape)) {
                this.ExitPhotoMode();
                return;
            }

            UserProfile userProfile = this.cameraRigController.localUserViewer.userProfile;
            Player inputPlayer = this.cameraRigController.localUserViewer.inputPlayer;

            float mouseLookSensitivity = userProfile.mouseLookSensitivity;
            float mouseLookScaleX = userProfile.mouseLookScaleX;
            float mouseLookScaleY = userProfile.mouseLookScaleY;

            if(Input.GetMouseButton(1))
                this.cameraState.fov = Mathf.Clamp(this.Camera.fieldOfView + (mouseLookSensitivity * Time.unscaledDeltaTime * inputPlayer.GetAxisRaw(3) * -10.0f), 4f, 120f);
            else if(Input.GetMouseButton(2)) {
                this.cameraState.roll += -mouseLookScaleX * mouseLookSensitivity * Time.unscaledDeltaTime * inputPlayer.GetAxisRaw(2) * 10.0f;
            } else {
                float deltaMouseX = mouseLookScaleX * mouseLookSensitivity * Time.unscaledDeltaTime * inputPlayer.GetAxisRaw(RewiredConsts.Action.AimHorizontalMouse);
                float deltaMouseY = mouseLookScaleY * mouseLookSensitivity * Time.unscaledDeltaTime * inputPlayer.GetAxisRaw(RewiredConsts.Action.AimVerticalMouse);

                this.ConditionalNegate(ref deltaMouseX, userProfile.mouseLookInvertX);
                this.ConditionalNegate(ref deltaMouseY, userProfile.mouseLookInvertY);

                float currentRollRadian = this.cameraState.roll * Mathf.Deg2Rad;
                this.cameraState.yaw += this.cameraState.fov * (deltaMouseX * Mathf.Cos(currentRollRadian) - deltaMouseY * Mathf.Sin(currentRollRadian));
                this.cameraState.pitch += this.cameraState.fov * (-deltaMouseY * Mathf.Cos(currentRollRadian) - deltaMouseX * Mathf.Sin(currentRollRadian));
                this.cameraState.pitch = Mathf.Clamp(this.cameraState.pitch, -89f, 89f);
            }
            // this.desiredCameraState.rotation = Quaternion.Euler(Mathf.Clamp(this.desiredCameraState.rotation.eulerAngles.x + deltaYaw, -89f, 89f), this.desiredCameraState.rotation.eulerAngles.y + deltaPitch, this.desiredCameraState.rotation.eulerAngles.z + deltaRoll);

            Vector3 moveVector = new Vector3(inputPlayer.GetAxisRaw(RewiredConsts.Action.MoveHorizontal), 0.0f, inputPlayer.GetAxisRaw(RewiredConsts.Action.MoveVertical));
            if(Input.GetKey(KeyCode.Q))
                moveVector.y -= 1;
            if(Input.GetKey(KeyCode.E))
                moveVector.y += 1;
            moveVector.Normalize();

            if(Input.GetKey(KeyCode.LeftShift)) {
                moveVector *= cameraSprintMultiplier;
            }
            if(Input.GetKey(KeyCode.LeftControl)) {
                moveVector *= cameraSlowMultiplier;
            }
            this.cameraState.position += cameraState.Rotation * moveVector * Time.unscaledDeltaTime * cameraSpeed;

            this.Camera.transform.position = this.cameraState.position;
            Quaternion newRotation = this.cameraState.Rotation;
            if(Mathf.Abs(newRotation.eulerAngles.z) < 2.0) {
                newRotation = newRotation.WithEulerAngles(null, null, 0f);
            }
            this.Camera.transform.rotation = newRotation;
            this.Camera.fieldOfView = this.cameraState.fov;            
        }

        public void GetCameraState(CameraRigController cameraRigController, ref CameraState cameraState) {
        }

        void ConditionalNegate(ref float value, bool condition) {
            value = condition ? -value : value;
        }

        public bool IsHudAllowed(CameraRigController cameraRigController) {
            return false;
        }

        public bool IsUserControlAllowed(CameraRigController cameraRigController) {
            return false;
        }

        public bool IsUserLookAllowed(CameraRigController cameraRigController) {
            return false;
        }

    }
}
 