# Risk of Rain 2 - PhotoMode Mod

[![Discord](https://img.shields.io/discord/745162241359478894?color=7289DA&label=modding%20discord&logo=discord&logoColor=white)](https://discord.gg/HbqdgMG)
[![source code](https://img.shields.io/static/v1?label=gitlab&logo=gitlab&message=source%20code&color=FCA121)](https://gitlab.com/cwmlolzlz/ror2skillsplusplus/-/tree/master)

This mod adds a free camera with the ability to move, rotate, and adjust FOV.
In the pause menu there is the "Photo Mode" button to enter. Pressing escape will resume the game and reset the camera back to your character.
The mod will try and turn off as many HUD elements as possible.
Forgive me if I have missed any.

I now have a [discord server](https://discord.gg/HbqdgMG) specifically for my Risk of Rain 2 mods. Come on by and give some feedback.

## Controls

The basics of camera movement and rotation are based of your game configured keybindings.

* Shift - Increased camera speed while held
* Left control - Reduced camera speed while held
* Right mouse - Vertical mouse movements adjust field of view while held
* Middle mouse - Horizontal mouse movements roll the camera while held

## Changelog

Version 1.1.0

- Update dependancies to work with Risk of Rain 2 launch release

Version 1.0.0

- Initial release
